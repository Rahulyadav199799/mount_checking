#!/bin/bash

diskname=$(sudo lsblk | grep "9.1T" | awk '{print $1}')
mountpoint1=$(df -h | grep "${diskname}1" | awk '{print $6}')
mountpoint2=$(df -h | grep "${diskname}2" | awk '{print $6}')

if [ -z "$diskname" ]; then
  echo "[Error]: 10Gb Drive is not found. Please check if the disk is connected."
  exit 1
fi

# Check if mountpoint1 is mounted
if [ -n "$mountpoint1" ]; then
  echo "$diskname is mounted to $mountpoint1."
else
  echo "$diskname is not mounted at $mountpoint1."
  exit 0
fi

# Check if mountpoint2 is mounted
if [ -n "$mountpoint2" ]; then
  echo "$diskname is mounted to $mountpoint2."
else
  echo "$diskname is not mounted at $mountpoint2."
  exit 0
fi
